package com.yun.demo.fallback;

import com.yun.demo.client.UserClient;
import com.yun.demo.entity.User;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by qingyun.yu on 2018/9/2.
 */
@Component
public class UserClientFallback implements UserClient{

    @Override
    public User getUser(User user) {
        System.out.println("熔断：" + user);
        User userRes = new User();
        userRes.setAge(100);
        userRes.setUserName("err");
        return userRes;
    }

    @Override
    public String create(User user) {
        System.out.println("熔断：" + user);
        return "error";
    }

    @Override
    public String list(String arg, String key) {
        return "error";
    }

    @Override
    public String getUserByUserId(String userId) {
        return "error";
    }

    @Override
    public String uploadFile(MultipartFile file) {
        return "error";
    }
}
