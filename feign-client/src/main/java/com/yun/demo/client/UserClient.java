package com.yun.demo.client;

import com.yun.demo.annotation.GetParam;
import com.yun.demo.entity.User;
import com.yun.demo.fallback.UserClientFallback;
import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by qingyun.yu on 2018/9/2.
 */
@FeignClient(name = "feign-service", fallback = UserClientFallback.class, configuration = UserClient.MultipartSupportConfig.class)
public interface UserClient {
    /*
        @RequestMapping(value = "user", method = RequestMethod.GET, consumes = "application/json")
    */
    @RequestMapping(value = "user", method = RequestMethod.GET)
    User getUser(@GetParam User user);

    @RequestMapping(value = "user", method = RequestMethod.POST)
    String create(User user);

    @RequestMapping(value = "users", method = RequestMethod.GET)
    String list(@RequestParam("arg") String arg, @RequestParam("key") String key);

    @RequestMapping(value = "user/{userId}", method = RequestMethod.GET)
    String getUserByUserId(@PathVariable("userId") String userId);
    /*@RequestLine("GET user/{userId}")
    String getUserByUserId(@Param("userId") String userId);*/

    @RequestMapping(value = "user/file", method = RequestMethod.PATCH,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    String uploadFile(@RequestPart("file") MultipartFile file);

    class MultipartSupportConfig {
        @Bean
        public Encoder feignFormEncoder() {
            return new SpringFormEncoder();
        }
    }
}
