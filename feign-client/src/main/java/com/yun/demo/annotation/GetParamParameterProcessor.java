package com.yun.demo.annotation;

import feign.MethodMetadata;
import org.springframework.cloud.openfeign.AnnotatedParameterProcessor;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;

/**
 * Created by qingyun.yu on 2018/9/4.
 */
@Component
public class GetParamParameterProcessor implements AnnotatedParameterProcessor {
    private static final Class<GetParam> ANNOTATION = GetParam.class;
    @Override
    public Class<? extends Annotation> getAnnotationType() {
        return ANNOTATION;
    }

    @Override
    public boolean processArgument(AnnotatedParameterContext context, Annotation annotation, Method method) {
        int parameterIndex = context.getParameterIndex();
        Class parameterType = method.getParameterTypes()[parameterIndex];
        MethodMetadata data = context.getMethodMetadata();
        this.setTemplate(parameterType, data, context);
        return true;
    }

    private void setTemplate(Class clazz, MethodMetadata data, AnnotatedParameterContext context) {
        Class superClass = clazz.getSuperclass();
        if(!"Object".equals(superClass.getSimpleName())) {
            this.setTemplate(superClass, data,context);
        }
        Field[] fields = clazz.getDeclaredFields();
        for(Field field: fields) {
            String name = field.getName();
            context.setParameterName(name);
            Collection query = context.setTemplateParameter(name, (Collection)data.template().queries().get(name));
            data.template().query(name, query);
        }
    }
}
