package com.yun.demo.annotation;

import java.lang.annotation.*;

/**
 * Created by qingyun.yu on 2018/9/4.
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GetParam {
    Class value() default Object.class;
}
