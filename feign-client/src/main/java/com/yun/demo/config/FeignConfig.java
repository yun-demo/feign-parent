package com.yun.demo.config;

import org.springframework.cloud.openfeign.annotation.PathVariableParameterProcessor;
import org.springframework.cloud.openfeign.annotation.RequestHeaderParameterProcessor;
import org.springframework.cloud.openfeign.annotation.RequestParamParameterProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by qingyun.yu on 2018/9/4.
 */
@Configuration
public class FeignConfig {
    @Bean
    public PathVariableParameterProcessor getPathVariableParameterProcessor() {
        return new PathVariableParameterProcessor();
    }

    @Bean
    public RequestParamParameterProcessor getRequestParamParameterProcessor() {
        return new RequestParamParameterProcessor();
    }

    @Bean
    public RequestHeaderParameterProcessor getRequestHeaderParameterProcessor() {
        return new RequestHeaderParameterProcessor();
    }
}