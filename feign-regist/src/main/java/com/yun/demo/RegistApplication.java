package com.yun.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Created by qingyun.yu on 2018/9/2.
 */
@EnableEurekaServer
@SpringBootApplication
public class RegistApplication {
    public static void main(String... args) {
        SpringApplication.run(RegistApplication.class, args);
    }
}
