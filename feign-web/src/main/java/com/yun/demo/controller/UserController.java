package com.yun.demo.controller;

import com.yun.demo.client.UserClient;
import com.yun.demo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by qingyun.yu on 2018/9/2.
 */
@RestController
public class UserController {
    @Autowired
    private UserClient userClient;

    @RequestMapping(value = "bff/user", method = RequestMethod.GET)
    public User getUser(User user) {
        return userClient.getUser(user);
    }

    @RequestMapping(value = "bff/user", method = RequestMethod.POST)
    public String create(User user) {
        return userClient.create(user);
    }

    @RequestMapping(value = "bff/users", method = RequestMethod.GET)
    public String list(String arg, String key) {
        return userClient.list(arg, key);
    }

    @RequestMapping(value = "bff/user/{userId}", method = RequestMethod.GET)
    public String getUserByUserId(@PathVariable String userId) {
        return userClient.getUserByUserId(userId);
    }

    @RequestMapping(value = "bff/user/file", method = RequestMethod.PATCH)
    public String uploadFile(MultipartFile file) {
        return userClient.uploadFile(file);
    }
}
