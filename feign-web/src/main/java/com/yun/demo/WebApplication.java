package com.yun.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Created by qingyun.yu on 2018/9/2.
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = "com.yun.demo")
public class WebApplication {
    public static void main(String... args) {
        SpringApplication.run(WebApplication.class, args);
    }
}
