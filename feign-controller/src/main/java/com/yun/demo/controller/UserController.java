package com.yun.demo.controller;

import com.yun.demo.entity.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by qingyun.yu on 2018/9/2.
 */
@RestController
public class UserController {

    @RequestMapping(value = "user", method = RequestMethod.GET)
    public User getUser(User user) {
        return user;
    }

    @RequestMapping(value = "user", method = RequestMethod.POST)
    public String create(@RequestBody User user) {
        System.out.println(user);
        return user.toString();
    }

    @RequestMapping(value = "users", method = RequestMethod.GET)
    public String list(String arg, String key) {
        return arg + ":" + key;
    }

    @RequestMapping(value = "user/{userId}", method = RequestMethod.GET)
    public String getUserByUserId(@PathVariable String userId) {
        return userId;
    }

    @RequestMapping(value = "user/{userId}", method = RequestMethod.DELETE)
    public String deleteUser(@PathVariable String userId) {
        return userId;
    }

    @RequestMapping(value = "user/file", method = RequestMethod.PATCH)
    public String uploadFile(MultipartFile file) {
        return file.getOriginalFilename();
    }
}
