package com.yun.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Created by qingyun.yu on 2018/9/2.
 */
@SpringBootApplication
@EnableEurekaClient
public class ControllerApplication {
    public static void main(String... args) {
        SpringApplication.run(ControllerApplication.class, args);
    }
}
