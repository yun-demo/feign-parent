package com.yun.demo.entity;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by qingyun.yu on 2018/9/2.
 */
public class User extends Age{
    private String userName;

    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return userName + " " + age;
    }
}
