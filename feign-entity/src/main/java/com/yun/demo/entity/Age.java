package com.yun.demo.entity;

/**
 * Created by qingyun.yu on 2018/9/4.
 */
public class Age {
    protected Integer age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
